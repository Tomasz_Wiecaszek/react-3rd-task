import './style.css'
import {useState} from 'react'
import {CharStats} from "../../context"
import {useContext} from 'react'
import Form from '../../components/Form'

const User =(props)=>{
    let {name,str,hp,speed,addStr,chngName,addHp,addSpeed}=useContext(CharStats);
    let [showStats, setshowStats] = useState(false);
    let fUserName=(nameUser)=>{chngName(name=nameUser);
        function getRandomInt(min, max) {
            min = Math.ceil(min);
            max = Math.floor(max);
            return Math.floor(Math.random() * (max - min)) + min;
          }
        addHp(hp=getRandomInt(1,10));
        addSpeed(speed=getRandomInt(1,10));

        console.log(name)};
    let fShowStats=()=>{setshowStats(!showStats)};
    return(
<div className="col-6">
    
    {showStats?<>
    <h1>Poziom: {props.poziom}</h1>
    <p>Name: {name}</p>
    <p>Strong: {str}</p>
    <p>Life: {hp}</p>
    <p>Speed: {speed}</p>
    <div onClick={()=>addStr(str+1)}>Add +1</div>
    </>:<Form f={fShowStats} f2={fUserName}/>
    }
    </div>
    )
}
export default User;