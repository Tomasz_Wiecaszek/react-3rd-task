import React from "react";


class Form extends React.Component{
    constructor(props){
        super(props);
        this.state={value:'Write your Nick Here...',showGameBool:false};
        this.handleChange=this.handleChange.bind(this);
        this.handleSubmit=this.handleSubmit.bind(this);
        this.changeF=this.props.f.bind(this);
        this.setUserName=this.props.f2.bind(this);
        
    }

     handleChange(e){
            this.setState({value:e.target.value});
        }
        handleSubmit(e){
            e.preventDefault();
            if(this.state.value.length<10){
            this.changeF();
            this.setUserName(this.state.value);}
            else{alert("Your nick may have 10 chars or less.")}
        }
        
        
        render(){
    return(
       
            <form onSubmit={this.handleSubmit}>
                <label>Imie: </label>
                <input name="name" type="text" value={this.state.value} onChange={this.handleChange}/>
                <input name="send" type="submit"/>
            </form>
    )}
}
export default Form;