import './style.css'
import {CharStats} from "../../context"
import {useContext} from 'react'

const Oponent =(props)=>{
    const {name,str,hp,speed}=useContext(CharStats);
    return(
<div className="col-6">
    <h1>Poziom: {props.poziom}</h1>
    <p>Name: {name}</p>
    <p>Strong: {str}</p>
    <p>Life: {hp}</p>
    <p>Speed: {speed}</p>
</div>
    )
}
export default Oponent;