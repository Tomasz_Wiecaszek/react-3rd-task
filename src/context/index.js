import React, {createContext, useState} from 'react';

export const CharStats=createContext({
    name:"",
    str:0,
    hp:0,
    speed:0,
    addStr:item=>{},
})


export const UserStatsProvider=({children})=>{
    const [name,chngName]=useState("Default");
    const [str,addStr]=useState(10);
    const [hp,addHp]=useState(10);
    const [speed,addSpeed]=useState(10);
    

    return(
        <CharStats.Provider
        value={{
            name,
            chngName,
            str,
            addStr,
            hp,
            addHp,
            speed,
            addSpeed
            
        }}
        >
            {children}
        </CharStats.Provider>
    )
}

export const OponentStatsProvider=({children})=>{
    const name="Demon";
    const str=10;
    const hp=12;
    

    return(
        <CharStats.Provider
        value={{
            name,
            str,
            hp
        }}
        >
            {children}
        </CharStats.Provider>
    )
}